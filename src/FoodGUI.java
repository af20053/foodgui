
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI {
    JPanel cardPanel;
    CardLayout layout;
    private JPanel root;

    private JButton Tempura;
    private JButton Ramen;
    private JButton Udon;
    public JTextArea OderedItemsList;
    private JButton maboTofuButton;
    private JButton gyozaButton;
    private JButton friedRiceButton;
    public JLabel TotalLabel;
    private JButton checkOutButton;
    private JButton shrimpsInChiliSauceButton;
    private JButton eggRollButton;
    private JButton foodbutton;
    private JButton desertbutton;
    private JPanel root3;
    private JButton Goma;
    private JButton sesameDumplingsButton;
    private JButton mangoPuddingButton;
    private JButton foodbutton1;
    private JButton desertbutton1;
    private JTextArea OderedItemsList2;
    private JLabel TotalLabel2;
    private JButton checkOutButton2;
    private JButton VanillaIce;
    private JButton EggTart;
    private JButton ChineseCastella;


    public FoodGUI() {
        cardPanel = new JPanel();
        layout = new CardLayout();
        cardPanel.setLayout(layout);
        cardPanel.add(root,"food");
        cardPanel.add(root3,"dessert");

        Ramen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen");
            }
        });
        shrimpsInChiliSauceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Shrimps in chili");
            }
        });
        eggRollButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Egg roll");
            }
        });
        maboTofuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Mabo tofu");
            }
        });
        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza");
            }
        });
        friedRiceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Fried rice");
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null, "Would you like to check out?", "Checkout Confirmation", JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you.The total price is " + totalvalue() + "yen.");
                    int total = 0;
                    TotalLabel.setText("Total " + total + " yen");
                    OderedItemsList.setText(null);
                    TotalLabel2.setText("Total " + total + " yen");
                    OderedItemsList2.setText(null);
                }
            }
        });

        foodbutton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                layout.show(cardPanel,"food");
            }

        });

        desertbutton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                layout.show(cardPanel,"dessert");
            }
        });
        foodbutton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                layout.show(cardPanel,"food");
            }

        });

        desertbutton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                layout.show(cardPanel,"dessert");
            }
        });

        Goma.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order2("Almond jelly");
            }
        });
        sesameDumplingsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order2("Sesame dumpling");
            }
        });

        mangoPuddingButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order2("Mango pudding");
            }
        });
        checkOutButton2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null, "Would you like to check out?", "Checkout Confirmation", JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you.The total price is " + totalvalue2() + "yen.");
                    int total = 0;
                    TotalLabel.setText("Total " + total + " yen");
                    OderedItemsList.setText(null);
                    TotalLabel2.setText("Total " + total + " yen");
                    OderedItemsList2.setText(null);
                }
            }
        });
        VanillaIce.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order2("Vanilla icecream");
            }
        });

        EggTart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order2("Egg tart");
            }
        });

        ChineseCastella.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order2("Chinese castella");
            }
        });
    }

    void order(String food) {
        int confirmation = JOptionPane.showConfirmDialog(null, "Would you like to order " + food + " ?", "Order Confirmation", JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
            JPanel root2 = new JPanel();
            JLabel HM= new JLabel("How many items?");
            SpinnerNumberModel model = new SpinnerNumberModel(1,1,null,1);
            JSpinner number = new JSpinner(model);
            JButton OKbutton = new JButton("OK");
            JLabel Large= new JLabel("How large?");
            ButtonGroup LargeValueGroup = new ButtonGroup();
            JRadioButton small = new JRadioButton("small -20yen");
            JRadioButton midium = new JRadioButton("midium");
            JRadioButton large = new JRadioButton("large +40yen");
            LargeValueGroup.add(small);
            LargeValueGroup.add(midium);
            LargeValueGroup.add(large);

            SpringLayout layout = new SpringLayout();
            root2.setLayout(layout);

            layout.putConstraint(SpringLayout.WEST,HM,5,SpringLayout.WEST,root2);
            layout.putConstraint(SpringLayout.NORTH,HM,5,SpringLayout.NORTH,root2);

            layout.putConstraint(SpringLayout.WEST,number,5,SpringLayout.WEST,root2);
            layout.putConstraint(SpringLayout.NORTH,number,5,SpringLayout.SOUTH,HM);

            layout.putConstraint(SpringLayout.WEST,Large,5,SpringLayout.WEST,root2);
            layout.putConstraint(SpringLayout.NORTH,Large,10,SpringLayout.SOUTH,number);

            layout.putConstraint(SpringLayout.WEST,small,5,SpringLayout.WEST,root2);
            layout.putConstraint(SpringLayout.NORTH,small,5,SpringLayout.SOUTH,Large);

            layout.putConstraint(SpringLayout.WEST,midium,5,SpringLayout.WEST,root2);
            layout.putConstraint(SpringLayout.NORTH,midium,5,SpringLayout.SOUTH,small);

            layout.putConstraint(SpringLayout.WEST,large,5,SpringLayout.WEST,root2);
            layout.putConstraint(SpringLayout.NORTH,large,5,SpringLayout.SOUTH,midium);

            layout.putConstraint(SpringLayout.EAST,OKbutton,0,SpringLayout.EAST,root2);
            layout.putConstraint(SpringLayout.SOUTH,OKbutton,0,SpringLayout.SOUTH,root2);

            HM.setPreferredSize(new Dimension(120,20));
            number.setPreferredSize(new Dimension(80,20));

            Large.setPreferredSize(new Dimension(80,20));

            small.setPreferredSize(new Dimension(100,20));
            midium.setPreferredSize(new Dimension(100,20));
            large.setPreferredSize(new Dimension(100,20));

            OKbutton.setPreferredSize(new Dimension(80,20));
            /*root2.setLayout(null);

            HM.setBounds(10,10,80,20);
            number.setBounds(10,30,80,20);

            Large.setBounds(10,70,80,20);

            small.setBounds(10,90,100,20);
            midium.setBounds(10,110,100,20);
            large.setBounds(10,130,100,20);

            OKbutton.setBounds(210,145,80,20);*/

            root2.add(HM);
            root2.add(number);
            root2.add(Large);
            root2.add(small);
            root2.add(midium);
            root2.add(large);
            root2.add(OKbutton);

            JFrame frame = new JFrame(food+" Option Menu");
            frame.setContentPane(root2);
            frame.pack();
            frame.setBounds(175,200,300,200);
            frame.setResizable(false);
            frame.setVisible(true);
            OKbutton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    JOptionPane.showMessageDialog(null, "Thank you for ordering " + food + "! It will be as soon as possible.");
                    String currentText = OderedItemsList.getText();
                    int total = totalvalue();
                    if(small.isSelected()){
                        int value=foodvalue(food)-20;
                        OderedItemsList.setText(currentText + "small " + food +" "+ value + "yen\n"+"number of items:"+(Integer) (number.getValue())+"\n");
                        OderedItemsList2.setText(currentText + "small " + food +" "+ value + "yen\n"+"number of items:"+(Integer) (number.getValue())+"\n");
                        total += (Integer) (number.getValue())*value;
                    }
                    else if(large.isSelected()){
                        int value=foodvalue(food)+40;
                        OderedItemsList.setText(currentText + "large " + food +" "+ value + "yen\n"+"number of items:"+(Integer) (number.getValue())+"\n");
                        OderedItemsList2.setText(currentText + "large " + food +" "+ value + "yen\n"+"number of items:"+(Integer) (number.getValue())+"\n");
                        total += (Integer) (number.getValue())*value;
                    }
                    else{
                        OderedItemsList.setText(currentText + food +" "+ foodvalue(food)  + "yen\n"+"number of items:"+(Integer) (number.getValue())+"\n");
                        OderedItemsList2.setText(currentText + food +" "+ foodvalue(food)  + "yen\n"+"number of items:"+(Integer) (number.getValue())+"\n");
                        total += (Integer) (number.getValue())*foodvalue(food);
                    }
                    TotalLabel.setText("Total " + total + " yen");
                    TotalLabel2.setText("Total " + total + " yen");
                    frame.setVisible(false);
                }
            } );
        }
    }
    void order2(String Dessert){
        int confirmation = JOptionPane.showConfirmDialog(null, "Would you like to order " + Dessert+ " ?", "Order Confirmation", JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
           /*JOptionPane.showMessageDialog(null, "Thank you for ordering " + Dessert + "! It will be as soon as possible.");
            String currentText = OderedItemsList2.getText();
            OderedItemsList.setText(currentText + Dessert +" "+ Dessertvalue(Dessert)  + "yen\n");
            OderedItemsList2.setText(currentText + Dessert +" "+ Dessertvalue(Dessert)  + "yen\n");
            int total = totalvalue2();
            total += Dessertvalue(Dessert);
            TotalLabel.setText("Total " + total + " yen");
            TotalLabel2.setText("Total " + total + " yen");*/
            JPanel root2 = new JPanel();
            JLabel HM= new JLabel("How many items?");
            SpinnerNumberModel model = new SpinnerNumberModel(1,1,null,1);
            JSpinner number = new JSpinner(model);
            JButton OKbutton = new JButton("OK");
            JLabel Large= new JLabel("How large?");
            ButtonGroup LargeValueGroup = new ButtonGroup();
            JRadioButton small = new JRadioButton("small -20yen");
            JRadioButton midium = new JRadioButton("midium");
            JRadioButton large = new JRadioButton("large +40yen");
            LargeValueGroup.add(small);
            LargeValueGroup.add(midium);
            LargeValueGroup.add(large);

            SpringLayout layout = new SpringLayout();
            root2.setLayout(layout);

            layout.putConstraint(SpringLayout.WEST,HM,5,SpringLayout.WEST,root2);
            layout.putConstraint(SpringLayout.NORTH,HM,5,SpringLayout.NORTH,root2);

            layout.putConstraint(SpringLayout.WEST,number,5,SpringLayout.WEST,root2);
            layout.putConstraint(SpringLayout.NORTH,number,5,SpringLayout.SOUTH,HM);

            layout.putConstraint(SpringLayout.WEST,Large,5,SpringLayout.WEST,root2);
            layout.putConstraint(SpringLayout.NORTH,Large,10,SpringLayout.SOUTH,number);

            layout.putConstraint(SpringLayout.WEST,small,5,SpringLayout.WEST,root2);
            layout.putConstraint(SpringLayout.NORTH,small,5,SpringLayout.SOUTH,Large);

            layout.putConstraint(SpringLayout.WEST,midium,5,SpringLayout.WEST,root2);
            layout.putConstraint(SpringLayout.NORTH,midium,5,SpringLayout.SOUTH,small);

            layout.putConstraint(SpringLayout.WEST,large,5,SpringLayout.WEST,root2);
            layout.putConstraint(SpringLayout.NORTH,large,5,SpringLayout.SOUTH,midium);

            layout.putConstraint(SpringLayout.EAST,OKbutton,0,SpringLayout.EAST,root2);
            layout.putConstraint(SpringLayout.SOUTH,OKbutton,0,SpringLayout.SOUTH,root2);

            HM.setPreferredSize(new Dimension(120,20));
            number.setPreferredSize(new Dimension(80,20));

            Large.setPreferredSize(new Dimension(80,20));

            small.setPreferredSize(new Dimension(100,20));
            midium.setPreferredSize(new Dimension(100,20));
            large.setPreferredSize(new Dimension(100,20));

            OKbutton.setPreferredSize(new Dimension(80,20));
            /*root2.setLayout(null);

            HM.setBounds(10,10,80,20);
            number.setBounds(10,30,80,20);

            Large.setBounds(10,70,80,20);

            small.setBounds(10,90,100,20);
            midium.setBounds(10,110,100,20);
            large.setBounds(10,130,100,20);

            OKbutton.setBounds(210,145,80,20);*/

            root2.add(HM);
            root2.add(number);
            root2.add(Large);
            root2.add(small);
            root2.add(midium);
            root2.add(large);
            root2.add(OKbutton);

            JFrame frame = new JFrame(Dessert+" Option Menu");
            frame.setContentPane(root2);
            frame.pack();
            frame.setBounds(175,200,300,200);
            frame.setResizable(false);
            frame.setVisible(true);
            OKbutton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    JOptionPane.showMessageDialog(null, "Thank you for ordering " + Dessert + "! It will be as soon as possible.");
                    String currentText = OderedItemsList2.getText();
                    int total = totalvalue();
                    if(small.isSelected()){
                        int value=Dessertvalue(Dessert)-20;
                        OderedItemsList.setText(currentText + "small " + Dessert +" "+ value + "yen\n"+"number of items:"+(Integer) (number.getValue())+"\n");
                        OderedItemsList2.setText(currentText + "small " + Dessert +" "+ value + "yen\n"+"number of items:"+(Integer) (number.getValue())+"\n");
                        total += (Integer) (number.getValue())*value;
                    }
                    else if(large.isSelected()){
                        int value=Dessertvalue(Dessert)+40;
                        OderedItemsList.setText(currentText + "large " + Dessert +" "+ value + "yen\n"+"number of items:"+(Integer) (number.getValue())+"\n");
                        OderedItemsList2.setText(currentText + "large " + Dessert +" "+ value + "yen\n"+"number of items:"+(Integer) (number.getValue())+"\n");
                        total += (Integer) (number.getValue())*value;
                    }
                    else{
                        OderedItemsList.setText(currentText + Dessert +" "+ Dessertvalue(Dessert)  + "yen\n"+"number of items:"+(Integer) (number.getValue())+"\n");
                        OderedItemsList2.setText(currentText + Dessert +" "+ Dessertvalue(Dessert)  + "yen\n"+"number of items:"+(Integer) (number.getValue())+"\n");
                        total += (Integer) (number.getValue())*Dessertvalue(Dessert);
                    }
                    TotalLabel.setText("Total " + total + " yen");
                    TotalLabel2.setText("Total " + total + " yen");
                    frame.setVisible(false);
                }
            } );
        }
    }


    int foodvalue(String food) {
        int result = 0;
        switch (food) {
            case "Ramen":
                result = 750;
                break;
            case "Shrimps in chili":
                result = 820;
                break;
            case "Egg roll":
                result = 330;
                break;
            case "Mabo tofu":
                result = 590;
                break;
            case "Gyoza":
                result = 280;
                break;
            case "Fried rice":
                result = 440;
                break;
        }
        return result;
    }
    int Dessertvalue(String Dessert) {
        int result = 0;
        switch (Dessert) {
            case "Almond jelly":
                result = 150;
                break;
            case "Sesame dumpling":
                result = 220;
                break;
            case "Mango pudding":
                result = 190;
                break;
            case "Vanilla icecream":
                result = 120;
                break;
            case "Egg tart":
                result = 380;
                break;
            case "Chinese castella":
                result = 550;
                break;
        }
        return result;
    }
    int totalvalue() {
        String TotalText = TotalLabel.getText();
        String[] totaltext = TotalText.split(" ");
        int total = Integer.parseInt(totaltext[1]);
        return total;
    }
    int totalvalue2() {
        String TotalText = TotalLabel2.getText();
        String[] totaltext = TotalText.split(" ");
        int total = Integer.parseInt(totaltext[1]);
        return total;
    }
    public static void main(String[] args) {
        /*JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);*/
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().cardPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setResizable(false);
        frame.setVisible(true);

    }
}
